# qsshbdy

A Python 3 package to create OB SSH based on observations.


## Overview

TBD


## Installation with anaconda3 or miniconda3

Clone the repository:
```
    git clone git@gitlab.com:FA12/qsshbdy.git qsshbdy
```

Run the install script:
```
    . qsshbdy/install.sh
```


## Documentation

1. TBD; probably Sphinx-based so we can generate it from the Python docstrings.

## License

Apache 2.0
