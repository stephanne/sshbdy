#!/bin/bash

set -e

# Run the script

if [ "$#" -ne 1 ]; then
  echo "Usage: $0 <optionsfile>"
  exit
else
  OPT=$1
fi

# run SSH bdy generation
python run_sshbdy.py   $OPT
