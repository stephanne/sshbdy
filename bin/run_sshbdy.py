import argparse
import os
import sys
import yaml

import sshbdy

parser = argparse.ArgumentParser()
# Required positional parameter
parser.add_argument("optfile", type=str, help="YAML file containing parameters.")
# Required parameters
parser.add_argument("--gauge_data", type=str, help="Directory with gauge data.")
parser.add_argument("--output_dir", type=str, help="Directory to store the corrected files.")
parser.add_argument("--start_date", type=str, help="Forcing start date.")
parser.add_argument("--end_date",   type=str, help="Forcing end date.")
parser.add_argument("--bdy_type",   type=str, help="boundary: W,E,S, or N.")
parser.add_argument("--nbdynodes",  type=str, help="Number of nodes along this boundary.")
parser.add_argument("--nz",         type=int, help="Number of vertical levels in the domain.")
parser.add_argument("--dt",         type=float, help="Time interval of bdy needed (in hours).")
parser.add_argument("--climatology", type=str, help="File name of temperature climatology")

args = parser.parse_args()

# Load parameters from YAML file
try:
    with open(args.optfile) as f:
        opt = yaml.safe_load(f)
except Exception as e:
    print("Error importing config file {} ({})".format(args.optfile,e))
    print("Usage: {} <input>".format(os.path.basename(__file__)))
    print("where <input> is a config file")
    sys.exit()

# Apply commandline options
if args.gauge_data  is not None: opt['gauge_data']  = args.gauge_data
if args.output_dir  is not None: opt['output_dir']  = args.output_dir
if args.start_date  is not None: opt['start_date']  = args.start_date
if args.end_date    is not None: opt['end_date']    = args.end_date
if args.bdy_type    is not None: opt['bdy_type']    = args.bdy_type
if args.nbdynodes   is not None: opt['nbdynodes']   = args.nbdynodes
if args.nz          is not None: opt['nz']          = args.nz
if args.dt          is not None: opt['dt']          = args.dt
if args.climatology is not None: opt['climatology'] = args.climatology


sshbdy.sshbdy(opt)
