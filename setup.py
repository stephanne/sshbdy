from setuptools import setup, find_packages

with open('README.md') as f:
    readme = f.read()

install_requires = [
    'numpy',
    'matplotlib',
    'pytz',
    'netCDF4',
    'pyyaml',
    'scipy'
]

setup(
    name='sshbdy',
    version='0.1.0',
    description='Python tools for creating OB SSH based on observations',
    long_description=readme,
    author='Stephanne Taylor, Maxim Krassovski, Michael Dunphy',
    url='https://gitlab.com/FA12/qsshbdy',
    install_requires=install_requires,
    packages=find_packages(exclude=('bin'))
)
