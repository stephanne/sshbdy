import os
import numpy as np
import datetime as dt
import netCDF4 as nc
import matplotlib.dates as dates
import csv
from scipy.interpolate import interp1d as i1d
import calendar

def sshbdy(opt):
    """ Generate daily bdy forcing files using gauge data.
    """
    # ensure output folder exists
    os.makedirs(opt['output_dir'], exist_ok=True)
    # get gauge data
    time,ssh = get_mirror(opt['gauge_data'])
    # daily time array
    dtime = time_range(opt['start_date'], opt['end_date'], 24)
    # go through days
    for date in dtime:
        process_one_day(time,ssh,date,opt)

def process_one_day(time,ssh,date,opt):
    """ For one file
    """
    # ST: If using >1 slice per hour, need to get closer to full 24 hours.
    #date_end = date + dt.timedelta(hours=23)
    if opt['dt'] < 1:
        n = 1/opt['dt']
        minoffset = 60 * (n-1) / n - 1
    else:
        minoffset = 0
    date_end = date + dt.timedelta(hours=23, minutes=minoffset)
    sdate0 = date.strftime('%Y%m%d %H:%M:%S')
    sdate1 = date_end.strftime('%Y%m%d %H:%M:%S')

    # hourly times
    # ST: not necessarily hourly data -- used 15 minutes previously.  Need to read in the dt parameter from namelist
    mtime = time_range(sdate0, sdate1, opt['dt'])

    # generate boundary ssh (time,bdynodes)
    #TODO Smooth at 1 hr before interpolating to hourly?
    mssh = np.interp(dates.date2num(mtime), dates.date2num(time), ssh) # (time,)
    # replicate to boundary nodes
    # Add the rimwidth tiling to get patches rather than lines
    mssh = np.tile(mssh[:,None],opt['nbdynodes']) # (time,bdynodes)
    nssh = np.tile(mssh[:,:,None],opt['rimwidth']) # (time,bdynodes)
    mssh = nssh - opt['offset']*np.ones(nssh.shape)
    # save file
    datetag = date.strftime('y%Ym%md%d')
    fname = os.path.join(opt['output_dir'], 'bdy_ssh/', 'ssh_'+opt['bdy_type']+'_'+datetag+'.nc')
    #save_netcdf_ssh(fname,mssh[:,None,:]) # pass array of shape (time,1,bdynodes)
    #save_netcdf_gen(fname,'ssh',mssh[:,None,:], 'm', 'Sea Surface Height', 'T')
    save_netcdf_gen(fname,'ssh',mssh[:,:,:], 'm', 'Sea Surface Height', 'T')

    # ST additions: other variables
    # First temperature (interpolated from climatology)
    itemp = interp_clim_temp (opt['climatology'], mtime, opt['nbdynodes'], opt['nz'], opt['rimwidth'])

    fname = os.path.join(opt['output_dir'],'bdy_ts/', 't_'+opt['bdy_type']+'_'+datetag+'.nc')
    #save_netcdf_gen(fname,'thetao',itemp[:,:,None,:], 'deg C', 'Temperature (climatology)', 'T')
    save_netcdf_gen(fname,'thetao',itemp[:,:,:,:], 'deg C', 'Temperature (climatology)', 'T')

    fname = os.path.join(opt['output_dir'],'bdy_uv/', 'u_'+opt['bdy_type']+'_'+datetag+'.nc')
    #save_netcdf_gen(fname,'uo',0.0*itemp[:,:,None,:], 'm/s', 'x velocity', 'U')
    save_netcdf_gen(fname,'uo',0.0*itemp[:,:,:,:], 'm/s', 'x velocity', 'U')

    fname = os.path.join(opt['output_dir'],'bdy_uv/', 'v_'+opt['bdy_type']+'_'+datetag+'.nc')
    #save_netcdf_gen(fname,'vo',0.0*itemp[:,:,None,:], 'm/s', 'y velocity', 'V')
    save_netcdf_gen(fname,'vo',0.0*itemp[:,:,:,:], 'm/s', 'y velocity', 'V')
    
    fname = os.path.join(opt['output_dir'],'bdy_ts/', 's_'+opt['bdy_type']+'_'+datetag+'.nc')
    #save_netcdf_gen(fname,'so',0.0*itemp[:,:,None,:], 'm/s', 'Salinity', 'T')
    save_netcdf_gen(fname,'so',0.0*itemp[:,:,:,:], 'm/s', 'Salinity', 'T')




#### code adapted from python-analysis-package
def time_range (startdate, enddate, interval, offset=0.0):
	"""Utility: Generates a consistently spaced time array from startdate to enddate
	            offset is used if required timestamps do not fall on the hour"""

	# dates are strings, and interval is in hours
	# convert to datetime object
	sd = numdate(startdate)
	ed = numdate(enddate)

	# get the number of total intervals (hourly, halfhourly, whatever) between the requested dates
	# use seconds so everything can be integers.
	window = int( (ed - sd).total_seconds() )
	nsec   = int (interval * 3600)

	# get an array of all the timestamps at the specified interval
	arr_time = np.asarray ([ sd + dt.timedelta(hours=float(offset)) + dt.timedelta(seconds=t) for t in range (0, window+nsec, nsec) ])
	
	return arr_time

def numdate (str_date):
	"""Utility: Takes a formatted date and returns a numerical value"""
	if len(str_date)==8:
		num_date = dt.datetime.strptime(str_date, '%Y%m%d')
		#num_date = calendar.timegm(time.strptime(str_date, '%Y%m%d'))
	elif len(str_date)==17:
		num_date = dt.datetime.strptime(str_date, '%Y%m%d %H:%M:%S')
	else:
		raise Exception('Unrecognized format for numdate: {}'.format(str_date))
	return num_date


#### code from bdy_salishsea_ssh_anom/bdy_salishsea_ssh_anom/io.py
def get_mirror(filename):
    """ Get series from  
    tc,hc = io.get_mirror(opt,'campbell')
    """
    t = nemo_nc_time(filename)
    with nc.Dataset(filename) as ncf:
        h = ncf.variables['waterlevel'][:].filled()
    return t,h


def nemo_nc_time(file_name):
    """ Get time from NEMO output file.
    
    Parameters
    ----------
    file_index : file name or file index in `source_files` list
    time_index : index of the time step in the file
    
    Returns
    -------
    time : array_like of datetime objects
    """
    with nc.Dataset(file_name) as ncf:
        timec = ncf.variables['time_counter'][:].filled()
#        dtc = np.unique(np.diff(timec))
#        if len(dtc)>1 or dtc<0:
#            raise RuntimeError('Non-uniform sampling for time_counter.')
        units,torigin = ncf.variables['time_counter'].getncattr('units').split(' since ')
    t0 = dt.datetime.strptime(torigin,'%Y-%m-%d %H:%M:%S')
    # treat both array nad scalar cases
    try:
        _ = iter(timec)
    except TypeError:
        # not iterable
        time = t0 + dt.timedelta(**{units:timec})
    else:
        # iterable
        time = np.array([t0 + dt.timedelta(**{units:timec1}) for timec1 in timec])
    
    return time



def interp_clim_temp (climatology, bdytime, bdynodes, nz, rimwidth):
	"""Interpolates a climatology using a spline fit to generate a temperature bdy file

	Parameters
	----------

	climatology: str
		csv file containing the temperature climatology (mm-dd, value,)
	bdytime: array_like
		array containing the time stamps that the climatology should be interpolated to 
	"""

	itemp = np.ones((bdytime.shape))

	# Climatology does not have a year associated with it
	# get the year from the bdytime input variable so python doesn't think it's the year of someone's lord
	refyear = bdytime[0].year

	if ( refyear % 4 == 0 and (refyear % 100 != 0 or refyear % 400 == 0) ):
		isleap = True
	else:
		isleap = False

	with open(climatology, 'r') as fid:
		next(fid)	# one header line (day of year, temperature,\n)
		data = list(csv.reader(fid))

		# Junk, kept in case want to return to date at x axis rather than day of year
		#a = [ data[i][0] for i in range(len(data)) ]
		#data_time  = np.array([ dt.datetime.strptime(refyear+'-'+i,'%Y-%m-%d') for i in a ])

		# Read in the data
		data_doy  = np.array([ float(data[i][0]) for i in range(len(data)) ] )
		data_temp = np.array([ float(data[i][1]) for i in range(len(data)) ] )

		# Adjust climatology for leap year
		if isleap:
			# There's already a buffer of extra days at the end of the file to 
			# account for year-spanning multiday calls, so all that needs doing is 
			# scootching the temperature values from  March onwards down one day
			ind = np.where(data_doy > 60)
			data_doy [ind] = data_doy[ind] + 1

		# Interpolate using a spline fit
		clim_temp = i1d(data_doy[:], data_temp[:], kind='cubic')

		# Convert bdytime to day of the year.  This will give integer day of year, can refine later
		bdytime_doy = np.array( [i.timetuple().tm_yday for i in bdytime])
		bdy_temp = clim_temp(bdytime_doy)

		#ztemp = np.tile(bdy_temp[:,None],50)  # (time,bdynodes)
		#mtemp = np.tile(ztemp[:,:,None],bdynodes) # (time,bdynodes)
		mtemp = np.tile(bdy_temp[:,None],bdynodes) # (time,bdynodes)
		ntemp = np.tile(mtemp[:,:,None],rimwidth) # (time,bdynodes)
		ztemp = np.tile(ntemp[:,:,:,None],nz)  # (time,bdynodes)

	return ztemp





def save_netcdf_gen(filename,varname, var, units, long_name, grid):
	"""General subroutine for saving any netcdf BDY files

	Parameters
	----------

	filename: str
		Output file name
	varname: str
		Name of variable to be saved in output file
	var: array_like
		Array of shape ('time_counter', 'yb', 'xbT')
	units: str
		Units of the variable
	long_name: str
		long name of the variable
	grid: str
		character indicating the grid the variable is on
	"""

	sz = var.shape
	print (sz)
	if (len(sz) == 3):
		is3d = False
	elif (len(sz) == 4):
		is3d = True
	else:
		print ('Error -- variable has unrecognized size')
		exit 

	#open netcdf file
	with nc.Dataset(filename, 'w') as outfile:

		#dimensions
		#outfile.createDimension('xbT', sz[2])
		#outfile.createDimension('yb', sz[1])
		outfile.createDimension('xbT', sz[1])
		outfile.createDimension('yb', sz[2])
		if (is3d):
			outfile.createDimension('zb', sz[3])
		outfile.createDimension('time_counter', None)
		if (is3d):
			vid = outfile.createVariable(varname, 'float32', ('time_counter','zb','yb','xbT'), zlib=True)
		else:
			# print the variable
			vid = outfile.createVariable(varname, 'float32', ('time_counter','yb','xbT'), zlib=True)
		vid.units     = units
		vid.long_name = long_name 
		vid.grid      = grid

		vid[:] = var[:]


