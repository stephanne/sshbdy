#!/bin/bash

# Create a new conda environment called `qsshbdy` and activate it:
conda create -y --name sshbdy
conda activate sshbdy

# Install pip and qsshbdy
conda install -y pip
pip install --editable sshbdy
